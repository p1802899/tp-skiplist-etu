#include "skip_liste.hpp"

#include <cassert>
#include <random>
#include <iostream>

int main() {

  { //insertion en s'assurant du tri
    SkipListe sl ;

    for (int i = 0; i < 20; i++)
    {
      sl.inserer(i);
    }
    sl.afficher() ;

    SkipCellule *find = sl.chercher(2);
    if (find) {
      std::cout << "On a trouvé la valeur " << find->valeur << std::endl;
    }
    else
    {
      std::cout << "La valeur n'a pas été trouvé" << std::endl;
    }

    assert(sl.test_tri()) ;

    sl.ajouter_niveau();

    sl.afficher();

    return 0;
  }

  /*
  { //insertion en n'assurant plus le tri
    SkipListe sl ;

    //generateurs aleatoires
    std::default_random_engine rd ;
    std::uniform_int_distribution<int> rand_int(0,99) ;

    for(int i = 20; i >= 0; --i) {
      sl.inserer(rand_int(rd)) ;
    }
    sl.afficher() ;

    assert(sl.test_tri()) ;
  }
  */

}
