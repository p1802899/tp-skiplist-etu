#include "skip_liste.hpp"

#include <iostream>

SkipListe::SkipListe() {
  /* la valeur est inutile, mais on met -2147483648 */

  /* la tête de la liste est la suivante de la sentinelle*/
  m_sentinelle = new SkipCellule(1<<31) ;
}

SkipListe::~SkipListe() {
  /* destruction recursive via le destructeur de SkipCellule */
  delete m_sentinelle ;
}

void SkipListe::inserer(int v) {
  SkipCellule* nlle = new SkipCellule(v) ;

  /* cette insertion n'est pas triee, c'est votre travail de le faire */

  /* pour trier : avancer dans la liste jusqu'a trouver une valeur plus grande
   * ou la fin de la liste, en gardant le curseur sur la precedente. Une fois arrive
   * sur la celule plus grande ou la fin de la liste, il faut inserer apres le
   * curseur (un peu comme au dessus pour inserer apres la sentinelle).
   *
   * initialiser un curseur sur la sentinelle
   * Tant que le curseur a une cellule suivante et que sa valeur est plus petite que v {
   *    avancer le curseur sur sa suivante
   * }
   * inserer apres le curseur
   *
   * */

  /* initialise le curseur sur la tête*/

  SkipCellule *curseur = m_sentinelle;

  while (curseur->suivante[0] && curseur->suivante[0]->valeur < v) {
    curseur = curseur->suivante[0];
  }

  //insertion
  nlle->suivante[0] = curseur->suivante[0];
  curseur->suivante[0] = nlle ;

}

SkipCellule* SkipListe::chercher(int v) {
  /* cette recherche ne fait rien, c'est votre travail de coder cette fonction */

  /* pour chercher dans une liste triee : avancer dans la liste tant qu'on est
   * sur une valeur strictement plus petite. Si on arrive sur une valeur égale,
   * on a trouve, si on arrive sur la fin de la liste ou une valeur plus grande,
   * on ne trouvera pas plus loin car la liste est (devrait etre) triee.
   *
   * initialiser un curseur sur la sentinelle
   * Tant que le curseut a uns cellule suivante et que sa valeur est plus petite que v {
   *   avancer le curseur sur sa suivante
   * }
   * Si le curseur n'est pas la sentinelle et a la valeur v
   *   le retourner
   * Sinon
   *   retourner nullptr
   *
   */

  SkipCellule *curseur = m_sentinelle;

  while (curseur->suivante[0] && curseur->suivante[0]->valeur < v) {
    curseur = curseur->suivante[0];
  }

  //insertion
  if (curseur->suivante[0] != m_sentinelle && curseur->suivante[0]->valeur == v)
  {
    return curseur->suivante[0];
  }
  else
  {
    return nullptr;
  }
}

bool SkipListe::test_tri() {
  //initialisation du curseur
  SkipCellule* courante = m_sentinelle->suivante[0] ;
  while(courante && courante->suivante[0]) {
    //on a deux cellules de suite qui existent
    if(courante->valeur > courante->suivante[0]->valeur) {
      //la premiere a une valeur plus grande que la seconde, ce n'est pas trie
      return false ;
    }
    courante = courante->suivante[0] ;
  }
  return true ;
}

void SkipListe::afficher() {
  std::cout << "niveau 0 : [ " ;
  //parcours lineaire du niveau 
  SkipCellule* courante = m_sentinelle->suivante[0] ;
  while(courante) {
    std::cout << courante->valeur << " " ;
    courante = courante->suivante[0];
  }
  std::cout << "]" << std::endl ;

  //si la sentinelle possède un deuxième niveau, alors on l'affiche
  if (m_sentinelle->suivante[1] != nullptr)
  {
    std::cout << "niveau 1 : [ ";
    courante = m_sentinelle->suivante[1];
    while (courante) {
      std::cout << courante->valeur << " ";
      courante = courante->suivante[1];
    }
    std::cout << "]" << std::endl;
  }
}

bool SkipListe::pile_ou_face() {
  //lancer la piece
  return m_piece(m_random) ;
}

void SkipListe::ajouter_niveau() {
  m_sentinelle->suivante.push_back(nullptr);

  //courante à initialisé sur la tête de la liste
  SkipCellule *courante = m_sentinelle->suivante[0];
  SkipCellule *precedente = m_sentinelle;

  while (courante != nullptr) {
    //tire à pile ou face
    bool res = pile_ou_face();

    if (res) {
      //si la cellule courante est sélectionné, on lui ajoute un niveau
      courante->suivante.push_back(nullptr);

      //le suivant de la cellule précédente pointe sur la cellule courante
      precedente->suivante[1] = courante;
      precedente = courante;
    }

    courante = courante->suivante[0];
  }
}